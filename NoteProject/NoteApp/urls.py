app_name = 'NoteApp'

from django.conf.urls import url
from .views import Login, Register, CreateNote

urlpatterns = [

    url(r'^login', Login.as_view(), name='sign-in'),
    url(r'^register', Register.as_view(), name='sign-up'),
    url(r'^createNote', CreateNote.as_view(), name='create-note'),
]

from rest_framework.authtoken.views import obtain_auth_token
urlpatterns += [
    url(r'^api-token-auth/', obtain_auth_token)
]
