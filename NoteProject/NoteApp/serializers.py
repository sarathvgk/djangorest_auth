from django.contrib.auth.models import User, Group
from .models import NoteUser, Note

from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ("id", "first_name", "last_name", "email", "username")

class NoteUserSerializer(serializers.ModelSerializer):

    user = UserSerializer(read_only=True, many=False)

    class Meta:
        model = NoteUser
        fields = ('user', 'profilePicURL')

class NoteSerializer(serializers.ModelSerializer):

    createdBy = NoteUserSerializer(read_only=True, many=False)

    class Meta:
        model = Note
        fields = ('noteId', 'noteText', 'createdBy')
