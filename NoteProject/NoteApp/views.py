from django.shortcuts import render
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.views import status, APIView

from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

from .models import NoteUser, Note
from .serializers import NoteUserSerializer, NoteSerializer
from django.contrib.auth.models import User, Group
from rest_framework.authtoken.models import Token

class Register(APIView):

    def post(self, request, *args, **kwargs):

        username = getValue(request, 'username')
        password = getValue(request, 'password')
        confirm_password = getValue(request, 'confirm_password')
        email = getValue(request, 'email')
        first_name = getValue(request, 'first_name')
        last_name = getValue(request, 'last_name')
        profile_image_url = getValue(request, 'profile_image_url')

        _user = User.objects.create(username=username,
                                    email=email,
                                    first_name=first_name,
                                    last_name=last_name)

        _user.set_password(password)
        _user.save()

        _noteUser = NoteUser.objects.create(user=_user,
                                        profilePicURL=profile_image_url)
        _noteUser.save()

        if _noteUser != None:
            _noteUserJSON = NoteUserSerializer(_noteUser, many=False).data

        return Response(data={'userInfo': _noteUserJSON}, status=status.HTTP_200_OK)

class Login(APIView):

    def post(self, request, *args, **kwargs):

        username = request.data.get("username")
        password = request.data.get("password")

        if username is None or password is None:
            return Response({'error': 'Please provide both username and password'},
                            status=HTTP_400_BAD_REQUEST)

        user = authenticate(username=username, password=password)

        if not user:
            return Response({'error': 'Invalid Credentials'},
                            status=HTTP_404_NOT_FOUND)

        # CREATE TOKEN FOR AUTHENTICATION *******
        # ****************************************

        token, _ = Token.objects.get_or_create(user=user)

        _noteUserJSON = NoteUserSerializer(user.note_user, many=False).data

        return Response({'userInfo':_noteUserJSON ,'token': token.key},
                        status=HTTP_200_OK)

class CreateNote(APIView):

    authentication_classes = [SessionAuthentication, BasicAuthentication, TokenAuthentication]
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        noteText = request.data.get("noteText")

        if noteText is None:
            return Response({'error': 'Please enter the note.'},
                            status=HTTP_400_BAD_REQUEST)

        user = request.user

        _note = Note.objects.create(noteText=noteText,
                                            createdBy=user.note_user)
        _noteJSON = NoteSerializer(_note, many=False).data

        return Response({'noteInfo': _noteJSON},
                        status=HTTP_200_OK)

def getValue(request, param):
    return request.data.get(param)
