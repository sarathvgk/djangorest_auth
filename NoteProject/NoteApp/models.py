from django.db import models

# Create your models here.
# -*- coding: utf-8 -*-

from django.contrib.auth.models import User, Group

class NoteUser(models.Model):

    user = models.OneToOneField(User, related_name='note_user', on_delete=models.CASCADE)
    profilePicURL = models.CharField(max_length=300, null=False)

class Note(models.Model):

    noteId = models.AutoField(primary_key=True)
    noteText = models.CharField(max_length=500, null=True)
    createdBy = models.ForeignKey(NoteUser, on_delete=models.CASCADE, null=False)



